// import express from 'express'
const express = require('express')

const app = express()

app.use(express.json())

const port = 8000

const emails = [
  {
    "id": 1,
    "topic": "Email 1 topic",
    "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci animi at consequatur cum cumque et illum ipsam labore magni nam, nesciunt nihil non nulla optio similique, sit unde vitae, voluptatibus!"
  },
  {
    "id": 2,
    "topic": "Email 2 topic",
    "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci animi at consequatur cum cumque et illum ipsam labore magni nam, nesciunt nihil non nulla optio similique, sit unde vitae, voluptatibus!"
  },
  {
    "id": 3,
    "topic": "Email 3 topic",
    "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci animi at consequatur cum cumque et illum ipsam labore magni nam, nesciunt nihil non nulla optio similique, sit unde vitae, voluptatibus!"
  },
  {
    "id": 4,
    "topic": "Email 4 topic",
    "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci animi at consequatur cum cumque et illum ipsam labore magni nam, nesciunt nihil non nulla optio similique, sit unde vitae, voluptatibus!"
  },
  {
    "id": 5,
    "topic": "Email 5 topic",
    "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci animi at consequatur cum cumque et illum ipsam labore magni nam, nesciunt nihil non nulla optio similique, sit unde vitae, voluptatibus!"
  },
  {
    "id": 6,
    "topic": "Email 6 topic",
    "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci animi at consequatur cum cumque et illum ipsam labore magni nam, nesciunt nihil non nulla optio similique, sit unde vitae, voluptatibus!"
  },
  {
    "id": 7,
    "topic": "Email 7 topic",
    "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci animi at consequatur cum cumque et illum ipsam labore magni nam, nesciunt nihil non nulla optio similique, sit unde vitae, voluptatibus!"
  },
  {
    "id": 8,
    "topic": "Email 8 topic",
    "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci animi at consequatur cum cumque et illum ipsam labore magni nam, nesciunt nihil non nulla optio similique, sit unde vitae, voluptatibus!"
  },
  {
    "id": 9,
    "topic": "Email 9 topic",
    "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci animi at consequatur cum cumque et illum ipsam labore magni nam, nesciunt nihil non nulla optio similique, sit unde vitae, voluptatibus!"
  }
]

const users = [
  {id: 1, login: 'Super Admin', password: 'admin'},
  {id: 2, login: 'User 1', password: 'user1'},
  {id: 3, login: 'User 2', password: 'user2'},
]

app.get('/api/emails', (req, res) => {
  res.send(emails)
})

app.get('/api/emails/:emailId', (req, res) => {
  res.send(emails.find(e => e.id === +req.params.emailId))
})

app.post('/api/login', (req, res) => {
  const {login, password} = req.body
  const user = users.find(u =>
    u.login.toLowerCase() === login.toLowerCase() &&
    u.password === password
  )
  if (user) {
    res.send(user);
  } else {
    res.sendStatus(401);
  }
})

app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})