import React, {Component} from 'react';
import deathStar from './death-star.svg';
import './Error500.scss';

class Error500 extends Component {
  render() {
    return (
      <div className='error-500'>
        <h2>Whoops... An error has occurred</h2>
        <h4>But we already sent droids to fix it</h4>
        <div>
          <img src={deathStar} alt='Death star'/>
        </div>
      </div>
    );
  }
}

export default Error500;