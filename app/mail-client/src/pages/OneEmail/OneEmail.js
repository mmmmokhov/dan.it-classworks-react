import React, {useEffect, useState} from 'react';
import {useParams, useHistory} from 'react-router-dom';
import Loader from "../../components/Loader/Loader";
import Email from "../../components/Email/Email";
import {connect} from "react-redux";
import {emailOperations, emailSelectors} from "../../store/email";

const OneEmail = ({getEmail, email, isLoading}) => {
  const {emailId} = useParams();
  const history = useHistory();

  useEffect(() => {
    getEmail(emailId);
  }, [emailId])

  if (isLoading) {
    return <Loader/>
  }

  return (
    <div>
      <Email email={email} showFull/>
      <button onClick={() => history.push(`/inbox/${emailId - 1}`)}>Previous</button>
      <button onClick={() => history.push(`/inbox/${+emailId + 1}`)}>Next</button>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    email: emailSelectors.getEmail()(state),
    isLoading: emailSelectors.emailLoading()(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getEmail: (emailId) => dispatch(emailOperations.fetchEmail(emailId))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(OneEmail);