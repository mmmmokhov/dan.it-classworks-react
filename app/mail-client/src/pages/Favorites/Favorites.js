import React, {Component, useEffect, useState} from 'react';
import Email from "../../components/Email/Email";
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import Loader from "../../components/Loader/Loader";
import {emailsSelectors} from "../../store/emails";

const Favorites = () => {
  const emails = useSelector(emailsSelectors.getEmails())
  const isLoading = useSelector(emailsSelectors.emailsLoading())
  const favorites = useSelector(emailsSelectors.getFavoriteEmails())
  const dispatch = useDispatch()

  useEffect(() => {
    axios('/api/emails')
      .then(res => {
        dispatch({type: 'mail-client/emails/SET_EMAILS', payload: res.data})
      })
  }, []);

  if (isLoading) {
    return <Loader/>
  }

  const emailCards = emails
    .filter(e => favorites.includes(e.id))
    .map(e => <Email key={e.id} email={e}/>)

  return (
    <div>
      {emailCards.length === 0 && <div>You don't have any favorite emails</div>}
      <ul>
        {emailCards}
      </ul>
    </div>
  );
}

export default Favorites;