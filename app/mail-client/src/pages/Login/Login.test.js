import { Login } from "./Login";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../../store/store";
import userEvent from "@testing-library/user-event";

describe("Login test", () => {
  test("Smoke Login", () => {
    render(
      <Provider store={store}>
        <Login />
      </Provider>
    );
  });

  test("Login test user with inputs", () => {
    const setUserMock = jest.fn();
    const { getByTestId } = render(<Login setUser={setUserMock} />);

    const loginInput = getByTestId("loginTest");
    const passwordInput = getByTestId("passwordTest");
    const buttonTest = getByTestId("btn");

    userEvent.type(loginInput, "user");
    userEvent.type(passwordInput, "password");

    expect(setUserMock).not.toHaveBeenCalled();

    userEvent.click(buttonTest);

    expect(setUser).toHaveBeenCalledTimes(1);
    expect(setUser).toHaveBeenCalledWith({
      login: "user",
      password: "password",
    });
  });
});
