import React, { useRef, useState } from "react";
import "./Login.scss";
import axios from "axios";
import { connect } from "react-redux";

export const Login = ({ setUser }) => {
  const [loginError, setLoginError] = useState(null);
  const loginRef = useRef();
  const passwordRef = useRef();

  const handleSubmitForm = (e) => {
    e.preventDefault();
    const login = loginRef.current.value;
    const password = passwordRef.current.value;
    setUser({ login, password });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const login = loginRef.current.value;
    const password = passwordRef.current.value;

    axios
      .post("/api/login", { login, password })
      .then((res) => setUser(res.data))
      .catch((err) => {
        console.error(err);
        setLoginError("Login or password is incorrect");
      });
  };

  return (
    <form onSubmit={handleSubmitForm}>
      {loginError && <div className="error">{loginError}</div>}
      <div>
        <input
          data-testid="loginTest"
          type="text"
          placeholder="Login"
          ref={loginRef}
          required
        />
      </div>
      <div>
        <input
          data-testid="passwordTest"
          type="password"
          placeholder="Password"
          ref={passwordRef}
          required
        />
      </div>
      <div>
        <button data-testid="btn" type="submit">
          Submit
        </button>
      </div>
    </form>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    setUser: (user) => dispatch({ type: "SET_USER", payload: user }),
  };
};

export default connect(null, mapDispatchToProps)(Login);
