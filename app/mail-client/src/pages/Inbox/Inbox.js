import React, {Component, useEffect, useState} from 'react';
import Email from "../../components/Email/Email";
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import Loader from "../../components/Loader/Loader";
import {emailsSelectors} from "../../store/emails";

const Inbox = () => {
  const emails = useSelector(emailsSelectors.getEmails())
  const isLoading = useSelector(emailsSelectors.emailsLoading())
  const dispatch = useDispatch()

  useEffect(() => {
    axios('/api/emails')
      .then(res => {
        dispatch({type: 'mail-client/emails/SET_EMAILS', payload: res.data})
      })
  }, []);

  if (isLoading) {
    return <Loader/>
  }

  const emailCards = emails.map(e => <Email key={e.id} email={e}/>)

  return (
    <div>
      {emails.length === 0 && <div>You don't have any emails</div>}
      <ul>
        {emailCards}
      </ul>
    </div>
  );
}

// Inbox.propTypes = {
//   // emails: PropTypes.arrayOf(PropTypes.object).isRequired
//   emails: PropTypes.arrayOf(PropTypes.shape({
//     id: PropTypes.number.isRequired,
//     topic: PropTypes.string.isRequired
//   })).isRequired
// }

export default Inbox;