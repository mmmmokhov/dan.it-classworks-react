import React from "react";
import "./Email.scss";
import PropTypes from "prop-types";
import Icon from "../Icon/Icon";
import { Link } from "react-router-dom";
import { emailsOperations, emailsSelectors } from "../../store/emails";
import { useDispatch, useSelector } from "react-redux";

// function Email () {
const Email = ({ email, showFull }) => {
  const dispatch = useDispatch();
  const isFavorite = useSelector(emailsSelectors.isEmailFavorite(email.id));

  return (
    <li className="email">
      <div>
        <Link to={`/inbox/${email.id}`}>{email.topic}</Link>
        <Icon
          data-testId="icon"
          type="star"
          filled={isFavorite}
          onClick={() => dispatch(emailsOperations.toggleFavorites(email.id))}
        />
      </div>
      {showFull && <div>{email.body}</div>}
    </li>
  );
};

// string, bool, func, object, array, symbol, number
// isRequired
// instanceOf, oneOf([1, 2, 3]), oneOfType([PropTypes.string, PropTypes.func]), arrayOf(PropTypes.number), shape(), exact()
Email.propTypes = {
  // email: PropTypes.object.isRequired,
  email: PropTypes.exact({
    id: PropTypes.number.isRequired,
    topic: PropTypes.string.isRequired,
    body: PropTypes.string,
  }).isRequired,
};

export default Email;
