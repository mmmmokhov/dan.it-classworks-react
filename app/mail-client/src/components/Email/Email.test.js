import Email from "./Email";
import { render, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Provider } from "react-redux";
import store from "../../store/store";
import configureStore from "redux-mock-store";
import thunk from "redux-thunk";
import { Component } from "react";
import types from "../../store/emails/types";

const middlewares = [thunk];
const mockStore = configureStore(middlewares);
let testStore;

const testEmail = {
  id: 1,
  topic: "topic",
  body: "body",
};

beforeEach(() => {
  testStore = mockStore({
    emails: {
      favorites: [1, 2],
    },
  });
});

jest.mock("react-router-dom", () => ({
  Link: "a",
  //   withRouter: (Component) => (props) => <Component {...props} />,
}));

jest.mock("../Icon/Icon", () => (props) => (
  <div onClick={props.onClick} data-testId="icon">
    icon
  </div>
));

describe("Test Email", () => {
  test("Smoke Email", () => {
    render(
      <Provider store={testStore}>
        <Email email={testEmail} />
      </Provider>
    );
  });

  test("Test Email toggleFavorite", () => {
    const { getByTestId } = render(
      <Provider store={testStore}>
        <Email email={testEmail} />
      </Provider>
    );

    const icon = getByTestId("icon");
    userEvent.click(icon);
    console.log(testStore.getActions());

    const expectedActios = [
      {
        type: types.TOGGLE_FAVORITES,
        payload: testEmail.id
      },
    ];
    expect(testStore.getActions()).toEqual(expectedActios)
  });
});
