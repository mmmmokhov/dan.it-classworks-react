import React from 'react';
import {NavLink} from "react-router-dom";
import './Sidebar.scss';

const Sidebar = () => {
  return (
    <ul>
      <li><NavLink exact to='/inbox' activeClassName='selected'>Inbox</NavLink></li>
      <li><NavLink exact to='/favorites' activeClassName='selected'>Favorites</NavLink></li>
      <li><NavLink exact to='/sent' activeClassName='selected'>Sent</NavLink></li>
      <li><NavLink exact to='/login' activeClassName='selected'>Login</NavLink></li>
    </ul>
  );
};

export default Sidebar;