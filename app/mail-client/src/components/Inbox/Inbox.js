import React, {Component} from 'react';
import Email from "../Email/Email";
import PropTypes from 'prop-types';

class Inbox extends Component {
  render() {
    const {emails = []} = this.props;

    const emailCards = emails.map(e => <Email key={e.id} email={e} />)

    return (
      <div>
        {emails.length === 0 && <div>You don't have any emails</div>}
        <ul>
          {emailCards}
        </ul>
      </div>
    );
  }
}

Inbox.propTypes = {
  // emails: PropTypes.arrayOf(PropTypes.object).isRequired
  emails: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    topic: PropTypes.string.isRequired
  })).isRequired
}

export default Inbox;
