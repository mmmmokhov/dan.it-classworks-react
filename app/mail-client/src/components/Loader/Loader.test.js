import { render } from "@testing-library/react";
import { Loader } from './Loader'


describe("Loader snapshot tests", () => {
    test("Simple Loader snapshot test with title", () => {
      const container = render(<Loader />);
      expect(container).toMatchSnapshot();
    });
  });
