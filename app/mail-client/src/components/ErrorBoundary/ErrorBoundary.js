import React, {Component} from 'react';
import Error500 from "../../pages/Error500/Error500";

class ErrorBoundary extends Component {
  state = {
    hasError: false
  }

  // componentDidCatch(error, errorInfo) {
  //   this.setState({hasError: true})
  //   axios.post('/error', {error, errorInfo})
  // }

  static getDerivedStateFromError(error) {
    return {hasError: true}
  }

  render() {
    const {children} = this.props
    const {hasError} = this.state

    if (hasError) {
      return <Error500 />
    }

    return children;
  }
}

export default ErrorBoundary;