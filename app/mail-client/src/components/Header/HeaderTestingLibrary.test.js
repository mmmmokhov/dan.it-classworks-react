import { Header } from "./Header";
import { render } from "@testing-library/react";

describe("HEADER testing-library TEST", () => {
  test("Smoke Header test", () => {
    render(<Header />);
  });

  test("Smoke Header test h3", () => {
    const { getByText } = render(<Header />);
    getByText("Header");
  });

  test("Smoke Header test h2", () => {
    const testTitle = "Test Title";
    const { getByText } = render(<Header title={testTitle} />);
    getByText(testTitle);
  });

  test("Smoke Header est user exists", () => {
    const user = {
      login: "login",
    };
    const { getByTestId } = render(<Header user={user} />);
    getByTestId("loginName");
  });
});

describe("Header snapshot tests", () => {
  test("Simple Header snapshot test", () => {
    const container = render(<Header />);
    expect(container).toMatchSnapshot();
  });

  test("Simple Header snapshot test with title", () => {
    const container = render(<Header title='TiT'/>);
    expect(container).toMatchSnapshot();
  });
});
