import { unmountComponentAtNode, render } from "react-dom";
import { Header } from "./Header";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe("HEADER TEST", () => {
  test("Smoke Header test", () => {
    render(<Header />, container);
  });

  test("Smoke Header test h3", () => {
    const testTitle = "Test Title";
    render(<Header title={testTitle} />, container);
    const title = document.getElementsByTagName("h2")[0];
    expect(title.textContent).toBe(testTitle);
  });

  test("Smoke Header est user exists", () => {
    const user = {
      login: "login",
    };
    render(<Header user={user} />, container);
    const title = document.getElementsByTagName("h4")[0];
    expect(title.textContent).toBe(user.login);
  });
});
