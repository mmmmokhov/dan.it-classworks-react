import React from 'react';
import './Header.scss';
import PropTypes from 'prop-types';
import {connect} from "react-redux";

export const Header = ({title = "Default title", user}) => {
  return (
    <div>
      <h1>1234</h1>
      <h3 id='header3'>Header</h3>
      <h2>{title}</h2>
      {user && <h4 data-testid='loginName'>{user.login}</h4>}
    </div>
  )
}

Header.propTypes = {
  title: PropTypes.string
}

// Header.defaultProps = {
//   title: "Default title"
// }

const mapStateToProps = (state) => {
  return {
    user: state.user.data
  }
}

export default connect(mapStateToProps)(Header);
