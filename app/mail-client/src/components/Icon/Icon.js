import React from 'react';
import * as Icons from '../../theme/icons';

const Icon = ({type, className = '', onClick, ...rest}) => {
  const iconJsx = Icons[type]

  if (!iconJsx) {
    return null;
  }

  return (
    <span className={`icon icon--type ${className}`} onClick={onClick}>
      {iconJsx({...rest})}
    </span>
  );
};

export default Icon;