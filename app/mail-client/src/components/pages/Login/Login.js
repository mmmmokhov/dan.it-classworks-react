import React, {useRef} from 'react';
import {Redirect} from "react-router-dom";

const Login = ({setUser, isLoggedIn}) => {
  const loginRef = useRef();
  const passwordRef = useRef();

  const handleSubmit = (e) => {
    e.preventDefault()
    const login = loginRef.current.value;
    const password = passwordRef.current.value;

    // axios.post('/api/login')
    //   .then(res => setUser(res.data))
    setUser({login, password});
  }

  // if (isLoggedIn) {
  //   return <Redirect to='/'/>
  // }

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <input type='text' placeholder='Login' ref={loginRef} required/>
      </div>
      <div>
        <input type='password' placeholder='Password' ref={passwordRef} required/>
      </div>
      <div>
        <button type='submit'>Submit</button>
      </div>
    </form>
  );
};

export default Login;
