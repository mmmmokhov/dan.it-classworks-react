import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import Email from '../../Email/Email';

const OneEmail = () => {
    const [email, setEmail] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    const {emailId} = useParams();

    useEffect(() => {
        // setIsLoading(true)
        axios(`/api/emails/${emailId}`)
        .then(res => {
            setEmail(res.data)
            setIsLoading(false)
        })
    }, [])

    return (
        <div>
            <Email email={email} showFull/>
        </div>
    );
};

export default OneEmail;
