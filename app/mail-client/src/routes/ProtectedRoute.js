import React from 'react';
import {Redirect, Route} from "react-router-dom";
import {useSelector} from "react-redux";

const ProtectedRoute = ({children, ...rest}) => {
  const user = useSelector(state => state.user.data)
  const isLoggedIn = !!user;
  return (
    <Route {...rest}>
      {isLoggedIn ? children : <Redirect to='/login' />}
    </Route>
  );
};

export default ProtectedRoute;