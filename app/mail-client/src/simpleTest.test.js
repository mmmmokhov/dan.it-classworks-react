const sum = (a, b) => a + b;

describe("Testing sum func", () => {
  test("1 + 2 expected 3", () => {
      expect(sum(1, 2)).toBe(3);
  });

//   test('1 + 2 expected 4 fail', () => {
//     expect(sum(1, 2)).toBe(4);
//   })
});
