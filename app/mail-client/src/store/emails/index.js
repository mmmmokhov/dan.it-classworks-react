import reducer from "./reducer";

export {default as emailsSelectors} from './selectors';
export {default as emailsOperations} from './operations';

export default reducer;