const SET_EMAILS = 'mail-client/emails/SET_EMAILS'
const TOGGLE_FAVORITES = 'mail-client/emails/TOGGLE_FAVORITES'
const SET_LOADING = 'mail-client/emails/SET_LOADING'

export default {
  SET_EMAILS, TOGGLE_FAVORITES, SET_LOADING
}
