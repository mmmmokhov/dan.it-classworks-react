import types from "./types";

const toggleFavorites = (emailId) => ({
  type: types.TOGGLE_FAVORITES,
  payload: emailId
})

export default {
  toggleFavorites
}