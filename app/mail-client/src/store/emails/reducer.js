import types from "./types";

const initialState = {
  data: [],
  isLoading: true,
  error: null,
  favorites: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_LOADING: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case types.SET_EMAILS: {
      return { ...state, data: action.payload, isLoading: false, error: null };
    }
    case types.TOGGLE_FAVORITES: {
      const emailId = action.payload;
      if (state.favorites.includes(emailId)) {
        return {
          ...state,
          favorites: state.favorites.filter((e) => e !== emailId),
        };
      } else {
        return { ...state, favorites: [...state.favorites, emailId] };
      }
    }
    default:
      return state;
  }
};

export default reducer;
