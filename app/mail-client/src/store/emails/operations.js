import actions from "./actions";

const {toggleFavorites} = actions;

export default {
  toggleFavorites
}