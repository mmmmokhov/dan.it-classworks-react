import reducer from "./reducer";
import types from "./types";

const state = {
  data: [],
  isLoading: false,
  error: null,
  favorites: [],
};

describe("Emails reducer test", () => {
  test("SET_EMAILS set data to data prop", () => {
    const testEmails = [{ id: 1 }, { id: 2 }, { id: 3 }];
    const action = {
      type: types.SET_EMAILS,
      payload: testEmails,
    };
    const newState = reducer(state, action);
    expect(newState.data).toBeTruthy();
  });

  test("SET_LOADING set isLoading true", () => {
    const action = {
      type: types.SET_LOADING,
    };
    const newState = reducer(state, action);
    expect(newState.isLoading).toBe(true);
  });

  test("TOGGLE_FAVORITES toggle fav", () => {
    const action = {
      type: types.TOGGLE_FAVORITES,
      payload: 1,
    };
    let newState = reducer(state, action);
    expect(newState.favorites).toContain(1);

    newState = reducer(newState, action);
    expect(newState.favorites).not.toContain(1);

  });
});
