const getEmails = () => (state) => state.emails.data
const emailsLoading = () => (state) => state.emails.isLoading
const isEmailFavorite = (emailId) => (state) => state.emails.favorites.includes(emailId)
const getFavoriteEmails = () => (state) => state.emails.favorites

export default {
  getEmails, emailsLoading, isEmailFavorite, getFavoriteEmails
}