import {combineReducers} from "redux";
import user from './user';
import emails from './emails';
import email from './email';

const reducer = combineReducers({
  user,
  emails,
  email
})

export default reducer