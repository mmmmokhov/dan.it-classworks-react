import reducer from "./reducer";

export {default as emailOperations} from './operations';
export {default as emailSelectors} from './selectors';

export default reducer