const getEmail = () => (state) => state.email.data
const emailLoading = () => (state) => state.email.isLoading

export default {
  getEmail, emailLoading
}