import types from "./types";

const initialState = {
  data: null,
  isLoading: true,
  error: null
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SAVE_EMAIL: {
      return {...state, data: action.payload}
    }
    case types.IS_LOADING: {
      return {...state, isLoading: action.payload}
    }
    default:
      return state;
  }
}

export default reducer