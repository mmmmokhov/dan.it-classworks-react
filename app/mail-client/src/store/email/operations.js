import actions from './actions';
import axios from "axios";

const fetchEmail = (emailId) => (dispatch, getState) => {
  dispatch(actions.setIsLoading(true))
  axios(`/api/emails/${emailId}`)
    .then(res => {
      dispatch(actions.saveEmail(res.data))
      dispatch(actions.setIsLoading(false))
    })
}

// const addEmail = (email) => (dispatch, getState) => {
//   const allEmails = [...getState().emails.data, email]
//   dispatch({type: 'SAVE_EMAILS', payload: allEmails})
// }

export default {
  fetchEmail
}