const initialState = {
  data: null,
  isLoading: true,
  error: null
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_USER': {
      return {...state, data: action.payload, isLoading: false}
    }
    default:
      return state;
  }
}

export default reducer