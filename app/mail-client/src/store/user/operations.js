import axios from "axios";

const registerUser = (user) => (dispatch, getState) => {
  // dispatch(actions.setIsLoading(true))
  axios(`/api/users/register`, user)
    .then(res => {
      // res.data = user
      dispatch(loginUser(user))

    })
}

const loginUser = (user) => (dispatch, getState) => {
  // dispatch(actions.setIsLoading(true))
  axios(`/api/users/login`, user)
    .then(res => {
      // dispatch(actions.saveEmail(res.data))
      // dispatch(actions.setIsLoading(false))
      // dispatch(saveToken(res.data))
    })
}

export default {
  loginUser, registerUser
}