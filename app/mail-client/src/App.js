import React from 'react';
import './App.scss';
import Header from './components/Header/Header';
import Footer from "./components/Footer/Footer";
import AppRoutes from "./routes/AppRoutes";
import Sidebar from "./components/Sidebar/Sidebar";

const App = () => {

  // if (isLoading) {
  //   return <Loader />
  // }

  return (
    <div className="App">
      <Header/>
      <Sidebar/>
      <AppRoutes/>
      <Footer/>
    </div>
  );
}

export default App;
