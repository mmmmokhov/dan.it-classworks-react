import "./App.css";
import CssModule from "./components/CssModule/CssModule";
import Test from "./components/MaterialUI/Test";
import StyledComponents from "./components/StyledComponents/StyledComponents";
import Header from "./components/MaterialUI/Header";

function App() {
  return (
    <div className="App">
      <Header />
      <CssModule />
      <StyledComponents />
      <Test />
    </div>
  );
}

export default App;
