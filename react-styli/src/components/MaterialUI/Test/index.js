import { Button } from "@material-ui/core";

const Test = () => {
  return (
    <Button variant="outlined" color="secondary">
      Primary
    </Button>
  );
};

export default Test;
