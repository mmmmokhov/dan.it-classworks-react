import React from "react";
import styles from "./CssModule.module.scss";

const CssModule = () => {
  return (
    <div className={styles.app}>
      Hello <div className='title'>World</div>
    </div>
  );
};

export default CssModule;
