import React from "react";
import styled, { css } from "styled-components";

const Button = styled.button`
  margin-top: 20px;
  width: 100px;
  height: 50px;
  background: green;
  border: 1px solid black;

  ${(props) =>
    props.color &&
    css`
      color: ${props.color};
    `}
`;

const StyledComponents = () => {
  return (
    <div>
      <Button color={'red'}>My button</Button>
    </div>
  );
};

export default StyledComponents;
