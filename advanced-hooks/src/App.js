import React, { useCallback, useMemo, useReducer, useState } from "react";
import "./App.css";
import Test from "./components/Test";
import emailReducer from "./reducers/emailReducers";
import Info from "./components/Info";
import ThemeProvider from "./providers/ThemeProvider";

const testEmails = [
  { id: 1, name: "Name 1" },
  { id: 2, name: "Name 2" },
];

function App() {
  const [number, setNumber] = useState(1);
  const [emails, dispatch] = useReducer(emailReducer, []);

  const loadEmails = useCallback(() => {
    dispatch({
      type: "SAVE_EMAILS",
      payload: [...testEmails],
    });
  }, []);

  const emailCards = useMemo(
    () => emails.map((e) => <div key={e.id}>{e.name}</div>),
    [emails]
  );

  return (
    <div>
      <ThemeProvider>
        <div className="App">{emailCards}</div>
        <div className="btn" onClick={loadEmails}>
          LOAD EMAILS
        </div>
        <div className="btn" onClick={() => setNumber(number + 1)}>
          INCREMENT
        </div>
        <div>{number}</div>
        <Test number={"number"} />
        <Info />
      </ThemeProvider>
    </div>
  );
}

export default App;
