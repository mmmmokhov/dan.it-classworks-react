import React, { useCallback, useEffect, useState } from "react";

export const useToggle = (initialValue) => {
    const [visible, setVisible] = useState(initialValue);

    const toggleVisible = useCallback(() => {
        setVisible(!visible)
    }, []);

    useEffect(() => {

    }, [visible]);

    return [visible, toggleVisible, setVisible];
}

export default useToggle;
