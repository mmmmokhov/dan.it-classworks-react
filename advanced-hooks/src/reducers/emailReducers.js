import React from "react";

const emailReducer = (state, action) => {
  switch (action.type) {
    case "SAVE_EMAILS": {
      return action.payload;
    }
    case "ADD_EMAIL": {
      return [...state, ...action.payload];
    }
    default:
      return state;
  }
};

export default emailReducer;
