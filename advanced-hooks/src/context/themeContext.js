import React from 'react';

export const themes = {
    light: {
        btnColor: 'orange',
    },
    dark: {
        btnColor: 'aqua',
    },
};

const ThemeContext = React.createContext(themes.dark);

ThemeContext.displayName = 'ThemeContext';

export default ThemeContext;
