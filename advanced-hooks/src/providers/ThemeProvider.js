import React, { useCallback, useState } from "react";
import ThemeContext, { themes } from "../context/themeContext";

function ThemeProvider(props) {
  const { children } = props;
  const [theme, setTheme] = useState(themes.dark);

  const toggleTheme = useCallback(() => {
    setTheme((prevState) =>
      prevState === themes.dark ? themes.light : themes.dark
    );
  }, []);

  return (
    <ThemeContext.Provider value={{ theme, toggleTheme }}>
      {children}
    </ThemeContext.Provider>
  );
}

export default ThemeProvider;
