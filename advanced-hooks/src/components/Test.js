import React, { memo, useContext } from "react";
import ThemeContext from "../context/themeContext";

function Test(props) {
  const { theme, toggleTheme } = useContext(ThemeContext);

  return (
    <div onClick={toggleTheme} style={{ backgroundColor: theme.btnColor }}>
      CHANGE THEME
    </div>
  );
}

export default memo(Test);
