import React from 'react';
import { useToggle } from '../hooks/useToggle';

const Info = (props) => {
    const [visible, toggleVisible] = useToggle(false);
    const [visibleCaptcha, toggleVisibleCaptcha] = useToggle(false);

    return (
        <div>
            <div>Lorem10 asjdnsjn jas naslkdn askldn askdsa lfa</div>
            {visible && <div>Lorem1231231</div>}
            {visibleCaptcha && <div>Captcha</div>}
            <div className='btn' onClick={toggleVisible}>more Info</div>
            <div className='btn' onClick={toggleVisibleCaptcha}>Show captcha</div>
        </div>
    );
};

export default Info;
