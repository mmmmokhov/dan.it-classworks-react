import React, {useRef, useState} from 'react';
import ControlledForm from './components/ControlledForm';
import Formik1 from './components/Formik1';
import UncontrolledForm from './components/UncontrolledForm';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&‘*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const App = () => {
  return (
    <div>
      <UncontrolledForm />
      <ControlledForm />
      <Formik1 />
    </div>
  );
};

export default App
