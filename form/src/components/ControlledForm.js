import React, { useEffect, useState } from 'react';

const EMAIL_REGEX =
	/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const ControlledForm = () => {
	const [values, setValues] = useState({
		login: '',
		password: '',
		confirmPassword: '',
	});
	const [errors, setErrors] = useState({});
	const [touched, setTouched] = useState({});

    useEffect(() => {
       setErrors(validateForm());
    }, [values])

	const handleSubmit = (e) => {
		e.preventDefault();

		const newErrors = validateForm();
		setErrors(newErrors);

		if (Object.keys(newErrors).length > 0) {
			return;
		}
		const { login, password, confirmPassword } = values;

		console.log(login, password, confirmPassword);
	};

	const validateForm = () => {
		const { login, password, confirmPassword } = values;

		let newErrors = {};

		if (!login) {
			newErrors.login = 'This field is required';
		} else if (!EMAIL_REGEX.test(login)) {
			newErrors.login = 'Please enter a valid email';
		}

		if (!password) {
			newErrors.password = 'This field is required';
		}
		if (!confirmPassword) {
			newErrors.confirmPassword = 'This field is required';
		}

		if (password !== confirmPassword) {
			newErrors.confirmPassword = 'Passwords do not match';
		}

		return newErrors;
	};

	const handleChange = (e) => {
		const { name, value } = e.target;
		setValues({ ...values, [name]: value });
	};

    const handleBlur =(e) => {
        const { name } = e.target;
		setTouched({ ...touched, [name]: true });
    };

	//console.log(values);

	return (
		<form onSubmit={handleSubmit}>
			<h2>Controlled form</h2>
			<div>
				<input
					defaultValue={'Log'}
					onChange={handleChange}
                    onBlur={handleBlur}
					value={values.login}
					name="login"
					type="text"
					placeholder="Login"
				/>
				{touched.login && errors.login && <div className="error">{errors.login}</div>}
			</div>
			<div>
				<input
					value={values.password}
					onChange={handleChange}
                    onBlur={handleBlur}
					name="password"
					type="password"
					placeholder="Password"
				/>
				{touched.password && errors.password && <div className="error">{errors.password}</div>}
			</div>
			<div>
				<input
					value={values.confirmPassword}
					onChange={handleChange}
                    onBlur={handleBlur}
					name="confirmPassword"
					type="password"
					placeholder="Confirm Password"
				/>
				{touched.confirmPassword && errors.confirmPassword && (
					<div className="error">{errors.confirmPassword}</div>
				)}
			</div>
			<div>
				<button type="submit">Submit</button>
			</div>
		</form>
	);
};

export default ControlledForm;
