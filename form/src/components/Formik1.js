import React, { useEffect, useState } from 'react';
import {Formik, Form, Field} from 'formik';
import MyInput from './MyInput';
import * as yup from 'yup';

const IS_REQUIRED = 'This field is required';


const schema = yup.object().shape({
    login: yup.string().required('IS_REQUIRED').email('Please enter a valid email'),
    password: yup.string().required('IS_REQUIRED').min(3, 'Enter min 3 characters'),
    confirmPassword: yup.string().required('IS_REQUIRED').oneOf([yup.ref('password')], 'Passwords do not match'),
})

const Formik1 = () => {

    const handleSubmit = (values) => {
		console.log(values);
	};

	return (
        <Formik
        initialValues={{login: '', password: '', confirmPassword: ''}}
        onSubmit={handleSubmit}
        validationSchema={schema}
        >
        {(formikProps) => {
        return (

		<Form noValidate>
			<h2>Formik 1</h2>
			<div>
				<Field
                    component={MyInput}
					name="login"
					type="text"
					placeholder="Login"
				/>
			</div>
			<div>
				<Field
                    component={MyInput}
					name="password"
					type="password"
					placeholder="Password"
				/>
			</div>
			<div>
				<Field
                    component={MyInput}
					name="confirmPassword"
					type="password"
					placeholder="Confirm Password"
				/>
			</div>
			<div>
				<button type="submit">Submit</button>
			</div>
		</Form>
        );
    }}
        </Formik>
	);
};

export default Formik1;
