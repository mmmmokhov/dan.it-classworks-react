import React, { useRef, useState } from 'react';

const EMAIL_REGEX =
	/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const UncontrolledForm = () => {
	const [errors, setErrors] = useState({});
	const loginRef = useRef();
	const passwordRef = useRef();
	const confirmPasswordRef = useRef();

	const handleSubmit = (e) => {
		e.preventDefault();

		const newErrors = validateForm();
		setErrors(newErrors);

		if (Object.keys(newErrors).length > 0) {
			return;
		}

		const login = loginRef.current.value;
		const password = passwordRef.current.value;
		const confirmPassword = confirmPasswordRef.current.value;

		console.log(login, password, confirmPassword);
	};

	const validateForm = () => {
		const login = loginRef.current.value;
		const password = passwordRef.current.value;
		const confirmPassword = confirmPasswordRef.current.value;

		let newErrors = {};

		if (!login) {
			newErrors.login = 'This field is required';
		} else if (!EMAIL_REGEX.test(login)) {
			newErrors.login = 'Please enter a valid email';
		}

		if (!password) {
			newErrors.password = 'This field is required';
		}
		if (!confirmPassword) {
			newErrors.confirmPassword = 'This field is required';
		}

		if (password !== confirmPassword) {
			newErrors.confirmPassword = 'Passwords do not match';
		}

		return newErrors;
	};

	console.log('render');

	return (
		<form onSubmit={handleSubmit}>
			<h2>Uncontrolled form</h2>
			<div>
				<input name="login" type="text" placeholder="Login" ref={loginRef} />
				{errors.login && <div className="error">{errors.login}</div>}
			</div>
			<div>
				<input
					name="password"
					type="password"
					placeholder="Password"
					ref={passwordRef}
				/>
				{errors.password && <div className="error">{errors.password}</div>}
			</div>
			<div>
				<input
					name="confirmPassword"
					type="password"
					placeholder="Confirm Password"
					ref={confirmPasswordRef}
				/>
				{errors.confirmPassword && (
					<div className="error">{errors.confirmPassword}</div>
				)}
			</div>
			<div>
				<button type="submit">Submit</button>
			</div>
		</form>
	);
};

export default UncontrolledForm;
