import React, {useEffect} from 'react';
import Characters from "../Characters/Characters";
import {useParams} from 'react-router-dom';
import axios from "axios";
import Loader from "../Loader/Loader";
import {useDispatch, useSelector} from "react-redux";

const MovieDetails = () => {
  const dispatch = useDispatch();
  const isLoading = useSelector((state) => state.film.isLoading);
  const movie = useSelector((state) => state.film.data);
  const params = useParams();

  useEffect(() => {
    axios(`https://ajax.test-danit.com/api/swapi/films/${params.filmId}`)
      .then(res => {
        dispatch({type: 'SET_MOVIE', payload: res.data})
      })
  }, [])

  if (isLoading) {
    return <Loader />
  }

  return (
    <div>
      <div>{movie.name}
      </div>
      <div>Эпизод: {movie.episodeId}</div>
      <div>Титры: {movie.openingCrawl}</div>
      <div>Персонажи: <Characters characters={movie.characters}/></div>
    </div>
  );
};

export default MovieDetails;