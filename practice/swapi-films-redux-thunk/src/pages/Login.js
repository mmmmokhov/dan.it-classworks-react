import React, {useRef} from 'react';

const Login = ({setUser}) => {
  const loginRef = useRef()
  const passRef = useRef()

  const handleSubmit = (ev) => {
    ev.preventDefault()
    const log = loginRef.current.value
    const pass = passRef.current.value
    const user = {log, pass}
    setUser(user)
  }

  return (
    <div>
      <form onSubmit={handleSubmit} action="">
        <div>
          <input ref={loginRef} type="text"/>
        </div>
        <div>
          <input ref={passRef} type="password"/>
        </div>
        <div>
          <button >LOGIN</button>
        </div>
      </form>
    </div>
  );
};

export default Login;