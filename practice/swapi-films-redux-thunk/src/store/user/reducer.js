const initialState = {
    films : {
      data: [],
      isLoading: true,
      error: false
    },
    film : {
      data: null,
      isLoading: true,
      error: false
    },
    user: null
  }

  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case 'SET_MOVIES' : {
        return {...state, films: {...state.films, data: action.payload, isLoading: false}}
      }
      case 'SET_MOVIE' : {
        return {...state, film: {...state.film, data: action.payload, isLoading: false}}
      }
      default : {
        return state
      }
    }

  }
