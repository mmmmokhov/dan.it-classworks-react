import React, {Component} from 'react';
import Characters from "../Characters/Characters";

class Movie extends Component {
  state = {
    expanded : false
  }

  render() {

    const {movie} = this.props
    return (
      <li>
        <div>{movie.name}
          {!this.state.expanded && < button onClick={this.expandMovie}>Детальнее...</button>}
        </div>
        {this.state.expanded && <div>Эпизод: {movie.episodeId}</div>}
        {this.state.expanded && <div>Титры: {movie.openingCrawl}</div>}
        {this.state.expanded && <div>Персонажи: <Characters characters={movie.characters} /></div>}
      </li>
    );
  }

  expandMovie = () => this.setState({expanded: true})
}

export default Movie;