import logo from './logo.svg';
import './App.css';
import React from 'react';
import axios from "axios";
import Loader from "./components/Loader/Loader";
import MovieList from "./components/MovieList/MovieList";



class App extends React.Component {
  state = {
    isLoading : true,
    movies: []
  }

  componentDidMount() {
    axios("https://ajax.test-danit.com/api/swapi/films")
      .then(res => this.setState({movies: res.data, isLoading: false}))
  }

  render() {
    return (
      <div className="App">
        {this.state.isLoading && <Loader/>}
        <MovieList movies={this.state.movies}/>
      </div>
    );
  }
}

export default App;
