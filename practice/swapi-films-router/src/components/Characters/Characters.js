import React, {Component, useEffect, useState} from 'react';
import axios from "axios";
import Loader from "../Loader/Loader";

const Characters = ({characters}) => {
  const [loadedCharacters, setLoadedCharacters] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const requests = characters.map(c => axios(c))

    Promise.all(requests).then(res => {
      const names = res.map(r => r.data.name)
      setLoadedCharacters(names)
      setIsLoading(false)
    })
  }, [characters])

  return (
    <div>
      {isLoading && <Loader/>}
      {!isLoading && loadedCharacters.join(", ")}
    </div>
  );
}

export default Characters;