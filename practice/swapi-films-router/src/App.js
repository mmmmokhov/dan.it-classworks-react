import logo from './logo.svg';
import './App.css';
import React, {useEffect, useState} from 'react';
import axios from "axios";
import Loader from "./components/Loader/Loader";
import MovieList from "./components/MovieList/MovieList";
import AppRoutes from "./components/AppRoutes/AppRoutes";

const App = () => {
  return (
    <div className="App">
      <AppRoutes />
    </div>
  );
}

export default App;
