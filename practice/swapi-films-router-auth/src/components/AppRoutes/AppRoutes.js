import React from 'react';
import {Switch, Route, Redirect} from "react-router-dom";
import Movies from "../Movies/Movies";
import MovieDetails from "../MovieDetails/MovieDetails";
import Login from "../../pages/Login";
import ProtectedRoute from "./ProtectedRoute";

const AppRoutes = ({user,setUser}) => {
  const isLog = !!user
  return (
    <Switch>
      <Redirect from={'/'} exact to={'/films'} />
      {
        isLog
        &&
        <Redirect exact from={'/login'}  to={"/"}/>
      }
      <ProtectedRoute isLogin={isLog} exact path={'/films'}><Movies/></ProtectedRoute>
      <ProtectedRoute isLogin={isLog} exact path={'/films/:filmId'}><MovieDetails /></ProtectedRoute>
      <Route exact path={'/login'}><Login setUser={setUser}/></Route>
    </Switch>
  );
};

export default AppRoutes;