import React from 'react';

const Header = ({user, setUser}) => {
  const logOutUser = () => setUser(null)

  return (
    <header>
      {!!user && <h2>{user.log}</h2>}
      {user && <button onClick={logOutUser}>logOut</button>}
    </header>
  );
};

export default Header;