import React from 'react';
import './App.css';

class App extends React.Component {
  state = {
    procent: 50,
    step: 5,
  }

  render() {
    const {procent, step} = this.state
    return (
      <div className="App">
        <div>
          <button onClick={this.decreaseCounter}>-</button>
          {procent}%
          <button onClick={this.increaseCounter}>+</button>
        </div>
        <div>
          <button onClick={this.decreaseStep}>-</button>
          {step}
          <button onClick={this.increaseStep}>+</button>
        </div>
      </div>
    );
  }

  decreaseCounter = () => {
    this.setState((prev) => ({procent: Math.max(0, prev.procent - prev.step)}))
    // this.setState({procent: this.state.procent - 1})
    //
    // if (true) {
    //   this.setState((prev) => ({procent: prev.procent - 1}))
    // }
  }

  increaseCounter = () => {
    this.setState((prev) => ({procent: Math.min(100, prev.procent + prev.step)}))
  }

  decreaseStep = () => {
    if (this.state.step > 1) {
      this.setState((prev) => ({step: prev.step - 1}))
    }
  }
  increaseStep = () => {
    if (this.state.step < 10) {
      this.setState((prev) => ({step: prev.step + 1}))
    }
  }
}

export default App;
