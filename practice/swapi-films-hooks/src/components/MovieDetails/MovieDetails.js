import React, {useEffect, useState} from 'react';
import Characters from "../Characters/Characters";
import {useParams} from 'react-router-dom';
import axios from "axios";
import Loader from "../Loader/Loader";

const MovieDetails = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [movie, setMovie] = useState(null);

  const params = useParams();

  useEffect(() => {
    axios(`https://ajax.test-danit.com/api/swapi/films/${params.filmId}`)
      .then(res => {
        setMovie(res.data);
        setIsLoading(false);
      })
  }, [])

  if (isLoading) {
    return <Loader />
  }

  return (
    <div>
      <div>{movie.name}
      </div>
      <div>Эпизод: {movie.episodeId}</div>
      <div>Титры: {movie.openingCrawl}</div>
      <div>Персонажи: <Characters characters={movie.characters}/></div>
    </div>
  );
};

export default MovieDetails;