import React from 'react';
import {Switch, Route, Redirect} from "react-router-dom";
import Movies from "../Movies/Movies";
import MovieDetails from "../MovieDetails/MovieDetails";

const AppRoutes = () => {
  return (
    <Switch>
      <Redirect from={'/'} exact to={'/films'} />
      <Route exact path={'/films'}><Movies/></Route>
      <Route exact path={'/films/:filmId'}><MovieDetails /></Route>
    </Switch>
  );
};

export default AppRoutes;